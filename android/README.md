# openmw-dependencies

These are the links to the source code used in this release.

1. icu
-   https://github.com/unicode-org/icu/

2. Zlib
-   https://github.com/madler/zlib/

3. LibJpeg_Turbo
-   https://github.com/libjpeg-turbo/libjpeg-turbo/

4. LibPNG
-   http://prdownloads.sourceforge.net/libpng/

5. Freetype 2
-   https://download.savannah.gnu.org/releases/freetype/

6. LibXML
-   https://github.com/GNOME/libxml2/

7. OpenAL
-   https://github.com/kcat/openal-soft/

8. Boost
-   https://github.com/boostorg/boost/

9. FFmpeg
-   http://ffmpeg.org/releases/

10. SDL2
-   https://github.com/libsdl-org/SDL/

11. Bullet
-   https://github.com/bulletphysics/bullet3/

12. GL4ES
-   https://github.com/ptitSeb/gl4es

13. MYGUI
-   https://github.com/MyGUI/mygui/

14. LZ4
-   https://github.com/lz4/lz4/

15. LuaJIT
-   https://github.com/luaJit/LuaJIT/

16. Collada Dom
-   https://github.com/rdiankov/collada-dom/

17. OPENSCENEGRAPH
-   https://github.com/openscenegraph/OpenSceneGraph
-   Fork used in this release https://github.com/openmw/osg/
# openmw-dep

This repository's intent is to provide an automated way to build all dependencies
required for [OpenMW](https://github.com/openmw/openmw).

Currently, this just has the scripts for MacOS and Android.

# Prerequisites for MacOS

* Xcode, 15.* is recommended, although it's possible to build using an older Xcode version by passing macOS SDK version to CMake like this: `-DCMAKE_OSX_SYSROOT=macosx10.13` (corresponds to Xcode 9.*).
* CMake
* pkg-config
* yasm

# Prerequisites for Android
* docker

# Building & installing for MacOS

1. Clone the repo
1. Create build dir within repo `mkdir build`
1. Run CMake from within `build` directory. `sandbox-exec -f ../sandbox.sb cmake -DCMAKE_BUILD_TYPE=Release ..`

1. Build: `sandbox-exec -f ../sandbox.sb make -j4`

* Now all files should be in `./build/openmw-mac-deps`, you should specify this path while running CMake for OpenMW later

# Building & installing for Android

1. Clone the repo
2. Run docker from within the `android` directory. `sudo docker build -t premades .`
3. Create the docker image using `CTID=$(docker create premades)`
4. Copy the libraries out using `sudo docker cp -a $CTID:/build/files/ ./`

# Notes

Some of libraries and frameworks installed in the system paths (Homebrew formulae included) may be picked up during
a build and lead to including unexpected header files or linking with unexpected binaries.
For example, OpenSceneGraph tries to use libtiff if present.

To prevent that, MacOS sandboxing mechanism is used in the above

Please note that sandbox profile assumes that CMake, pkg-config & yasm are installed with Homebrew in default prefix.
